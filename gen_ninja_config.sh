#!/bin/bash

main() {
    local cc
    if [[ -z ${CC+x} ]]; then
        cc=/usr/bin/cc
    else
        cc="${CC}"
    fi

    echo "CC = ${cc}"

    local cflags=()
    local ldflags=()

    local debug_cflags=(-O1 -g -fno-optimize-sibling-calls -fno-omit-frame-pointer)

    if [[ ${SANITIZE} == "memory" ]]; then
        # shellcheck disable=SC2206
        cflags+=(
            ${debug_cflags[@]}
            -fsanitize=memory
            -fsanitize-memory-track-origins
            -fsanitize-recover=all
        )

        ldflags+=(
            -g
            -fsanitize=memory
            -fsanitize-recover=all
        )
    elif [[ ${SANITIZE} == "address" ]]; then
        # shellcheck disable=SC2054,SC2206
        cflags+=(
            ${debug_cflags[@]}
            -fsanitize=address,undefined,integer,nullability
            -fsanitize-address-use-after-scope
            -fsanitize-recover=all
        )

        # shellcheck disable=SC2054
        ldflags+=(
            -g
            -fsanitize=address,undefined,integer,nullability
            -fsanitize-recover=all
        )
    elif [[ ${DEBUG} == "1" ]]; then
        # shellcheck disable=SC2206
        cflags+=(${debug_cflags[@]})
    else
        cflags+=(-DNDEBUG)
    fi

    if [[ ${COVERAGE} == "1" ]]; then
        cflags+=(-fprofile-instr-generate -fcoverage-mapping)
        ldflags+=(-fprofile-instr-generate -fcoverage-mapping)
    fi

    if [[ ${PEDANTIC} == "1" ]]; then
        cflags+=(
            -Werror -Wpedantic -pedantic-errors
            -Wno-gnu-zero-variadic-macro-arguments
        )
    fi

    if [[ -n ${CFLAGS+x} ]]; then
        # shellcheck disable=SC2206
        cflags+=(${CFLAGS[@]})
    fi

    echo "CFLAGS =" "${cflags[@]}"

    if [[ -n ${LDFLAGS+x} ]]; then
        # shellcheck disable=SC2206
        ldflags+=(${LDFLAGS[@]})
    fi

    echo "LDFLAGS =" "${ldflags[@]}"

    local nvml_header_path
    if [[ -z ${NVML_HEADER_PATH+x} ]]; then
        nvml_header_path=/opt/cuda/targets/x86_64-linux/include
    else
        nvml_header_path="${NVML_HEADER_PATH}"
    fi

    echo "NVML_HEADER_PATH = ${nvml_header_path}"
}

main > config.ninja
