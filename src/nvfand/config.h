#ifndef NVFAND_CONFIG_H
#define NVFAND_CONFIG_H

#include <stddef.h>

enum ConfigErrorStage {
    CONFIG_ERROR_STAGE_CURVES_NAME,
    CONFIG_ERROR_STAGE_CURVES_POINTS,
    CONFIG_ERROR_STAGE_MAPPINGS_UUID,
    CONFIG_ERROR_STAGE_MAPPINGS_CURVE_NAME,

    // keep this last
    CONFIG_ERROR_STAGE_NONE
};

struct FanCurvePoint {
    // (y2 - y1) / (x2 - x1)
    // where y2 and x2 are respectively speed and temp
    // and y1 and x1 are speed and temp of the prev point
    // (or 0 for the first point)
    float lerp_factor;

    // in celsius
    unsigned int temp;

    // percentage 0-100
    unsigned int speed;
};

struct FanCurve {
    char* name;
    size_t name_len;
    struct FanCurvePoint* points;
    size_t points_size;
};

struct GpuCurveMapping {
    // can be obtained with nvidia-smi -L
    char* uuid;
    size_t uuid_len;

    // non owned
    struct FanCurve* curve;
};

struct Config {
    struct FanCurve* curves;
    size_t curves_size;
    struct GpuCurveMapping* mappings;
    size_t mappings_size;
    unsigned int interval;
};

int config_parse(struct Config* config, const char* path);

void config_free_from_stage(
    struct Config* config,
    enum ConfigErrorStage last_successful_stage
);

#define CONFIG_FREE(config) config_free_from_stage(config, CONFIG_ERROR_STAGE_NONE)

#endif
