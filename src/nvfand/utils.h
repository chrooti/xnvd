#ifndef NVFAND_UTILS_H
#define NVFAND_UTILS_H

#include <errno.h>
#include <string.h>
#include <sys/types.h>

#if defined(__GNUC__) || defined(__clang__)
#define ATTRIBUTE(x) __attribute__(x)
#else
#define ATTRIBUTE(x)
#endif

#define PRINT_SYSTEM_ERROR(function) \
    fprintf(stderr, #function " error: %s\n", strerror(errno))

ATTRIBUTE((unused))

static inline ssize_t strnncmp(const char* s1, const char* s2, size_t n1, size_t n2) {
    if (n1 != n2) {
        return (ssize_t) n1 - (ssize_t) n2;
    }

    return memcmp(s1, s2, n1);
}

#endif
